'use strict';

import "babel-polyfill";
import Vue from 'vue';
import App from './src/App.vue';
import 'buefy'
import 'buefy/lib/buefy.css'
import './main.css';
import 'vue2-animate/dist/vue2-animate.min.css'

new Vue({
  el: '#app',
  render: h => h(App)
});