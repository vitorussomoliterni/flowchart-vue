export default `

Id,Message,Shortened,Options,Targets
1,What do you feel like eating?,Eating,Indian|Japanese|Thai,2|3|4
2,Do you like spicy food?,Spicy,Yes|No,5|6
3,Do you like seafood?,Seafood,Yes|No,7|8
4,Do you like nuts?,Nuts,Yes|No,9|10
5,Order almost anything!,,,
6,Have some korma I guess.,,,
7,Sushi it is.,,,
8,How about soupy food?,Soup,Yes|No,11|12
9,Pad Thai should do,,,
10,Maybe some Pad See-ew?,,,
11,Have ramen!,,,
12,I’d suggest a nice donburi.,,,

`;