'use strict';

import RawData from './rawData';
import Papa from 'papaparse';

const parsingConfig = {
  header: true,
  trimHeaders: true,
  delimiter: ',',
  skipEmptyLines: true,
  dynamicTyping: true
};

const mapStringsToArray = (names, targets) => {
  const optionsArray = names ? names.split('|') : [];
  const targetsArray = targets ? targets.split('|') : [];
  let array = [];

  for(let i = 0; i < optionsArray.length; i++) {
    array.push({
        name: optionsArray[i],
        target: targetsArray[i]
      }
    );
  }

  return array;
}

// Parse csv string into json
const data = Papa.parse(RawData, parsingConfig).data;

let result = [];

data.forEach(d => {
  const array = mapStringsToArray(d.Options, d.Targets) || [];
  const shortMessage = d.Shortened || d.Message;

  result.push({
    id: d.Id,
    message: d.Message,
    shortened: shortMessage,
    options: array
  });
});

const sortedResult = result.sort((a, b) => a.id - b.id);

// Return array in descending order
export default sortedResult;